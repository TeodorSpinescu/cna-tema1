package Client;

import io.grpc.Channel;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import resouces.protos.shownames.GreeterGrpc;
import resouces.protos.shownames.NameRequest;
import resouces.protos.shownames.NoReply;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Client {
    private static final Logger logger = Logger.getLogger(Client.class.getName());

    private final GreeterGrpc.GreeterBlockingStub blockingStub;

    public Client(Channel channel) {
        blockingStub = GreeterGrpc.newBlockingStub(channel);
    }

    public void greet(String name) {
        logger.info("Will try to greet " + name + " ...");
        NameRequest request = NameRequest
                                .newBuilder()
                                .setName(name)
                                .build();
        NoReply response;
        try {
            response = blockingStub.sayHello(request);
        } catch (StatusRuntimeException e) {
            logger.log(Level.WARNING, "RPC failed: {0}", e.getStatus());
            return;
        }
        logger.info("Greeting: " + response);
    }

    public static void main(String[] args) throws Exception {
        String user;
        String target = "localhost:50051";

        ManagedChannel channel = ManagedChannelBuilder.forTarget(target)
                .usePlaintext()
                .build();
        System.out.println("Enter name: ");
        Scanner scanner = new Scanner(System.in);
        user = scanner.nextLine();
        try {
            Client client = new Client(channel);
            client.greet(user);
        } finally {
            channel.shutdownNow().awaitTermination(5, TimeUnit.SECONDS);
        }
    }
}