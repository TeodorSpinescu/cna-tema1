package Server.ServiceServer;

import io.grpc.stub.StreamObserver;
import resouces.protos.shownames.GreeterGrpc;
import resouces.protos.shownames.NameRequest;
import resouces.protos.shownames.NoReply;

public class Service extends GreeterGrpc.GreeterImplBase {
    @Override
    public void sayHello(NameRequest req, StreamObserver<NoReply> responseObserver) {
        System.out.println("Hello " + req.getName());
        NoReply.Builder reply = NoReply.newBuilder();
        responseObserver.onNext(reply.build());
        responseObserver.onCompleted();
    }
}

