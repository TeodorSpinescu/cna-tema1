package Server.MainServer;

import Server.Server;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        final Server server;
        server = new Server();
        server.start();
    }
}
