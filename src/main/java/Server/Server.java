package Server;

import Server.ServiceServer.Service;
import io.grpc.ServerBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;


public class Server {
    private io.grpc.Server server;

    private static final Logger logger = Logger.getLogger(Server.class.getName());

    public Server() {
        int port = 50051;
        server = ServerBuilder
                .forPort(port)
                .addService(new Service())
                .build();
    }

    public void start() throws IOException {
        int port = 50051;
        server.start();
        logger.info("Server started, listening on " + port);
        try {
            server.awaitTermination();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.err.println("*** server shut down");
    }

    private void stop() throws InterruptedException {
        if (server != null) {
            server.shutdown().awaitTermination(30, TimeUnit.SECONDS);
        }
    }
}

